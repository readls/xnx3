package com.xnx3.wangmarket;

import com.xnx3.ConfigManagerUtil;
import com.xnx3.net.AuthHttpUtil;
import com.xnx3.net.HttpResponse;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import net.sf.json.JSONObject;

public class Authorization
{
  public static Boolean copyright = Boolean.valueOf(false);
  public static String auth_id;
  private static String domain;
  private static int version;
  private static int softType = 1;
  private static int sleeptime;
  private static int gainNumber = 0;
  private static Thread thread;

  static
  {
    auth();
  }

  public static void setDomain(String setDomain)
  {
    if ((setDomain == null) || (setDomain.equals("")))
      domain = "null";
    else
      domain = setDomain;
  }

  public static void setVersion(Integer version)
  {
    if (version == null) {
      version = 0;
      return;
    }
    version = version.intValue();
  }

  public static void setSoftType(int softType) {
    softType = softType;
  }

  public static void auth()
  {
    if (thread == null) {
      if ((auth_id == null) || (auth_id.length() == 0))
      {
        ConfigManagerUtil c = ConfigManagerUtil.getSingleton("wangMarketConfig.xml");
        auth_id = c.getValue("authorize");
        if ((auth_id == null) || (auth_id.equals("")))
        {
          String className = "com.xnx3.j2ee.util.ApplicationPropertiesUtil";
          try {
            Class cla = Class.forName(className);
            Object invoke = null;
            try {
              invoke = cla.newInstance();

              Method m = cla.getMethod("getProperty", new Class[] { String.class });

              Object o = m.invoke(invoke, new Object[] { "authorize" });
              if ((o != null) && (!o.equals("null")))
                auth_id = o.toString();
            }
            catch (Exception e) {
              e.printStackTrace();
            }
          } catch (ClassNotFoundException e) {
            e.printStackTrace();
          }
        }
        if ((auth_id != null) && (auth_id.trim().length() == 64))
        {
          sleeptime = 1800000;
        }
        else {
          sleeptime = 86400000;
        }

      }

      thread = new Thread(new Runnable()
      {
        public void run() {
          try {
            Thread.sleep(6000L);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          while (true)
          {
            AuthHttpUtil http = new AuthHttpUtil();
            Map params = new HashMap();
            params.put("auth", Authorization.auth_id);
            params.put("domain", Authorization.domain);
            params.put("softType", Authorization.softType);
            params.put("version", Authorization.version);
            String requestUrl = "http://cloud.wscso.com/auth?auth=" + Authorization.auth_id + "&version=" + Authorization.version + "&softType=" + Authorization.softType + "&domain=" + Authorization.domain;

            HttpResponse hr = null;
            try {
              hr = http.post(requestUrl, params);
            } catch (Exception e) {
              System.out.println("authorization service exception, but does not affect the system, you can still feel free to use !");

              Authorization.copyright = Boolean.valueOf(false);
              return;
            }

            if (hr.getCode() == 200)
            {
              JSONObject json = JSONObject.fromObject(hr.getContent().trim());
              if (json.get("result") != null) {
                String result = json.getString("result");
                if (result.equals("1"))
                {
                  Authorization.copyright = Boolean.valueOf(false);
                }
                else {
                  Authorization.copyright = Boolean.valueOf(true);
                }
              }
              else
              {
                if (Authorization.gainNumber == 0) {
                  Authorization.copyright = Boolean.valueOf(false);
                }

                Authorization.gainNumber += 1;
              }
            }
            else
            {
              Authorization.copyright = Boolean.valueOf(false);
            }

            try
            {
              Thread.sleep(Authorization.sleeptime);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
        }
      });
      thread.start();
    }
  }

  public static void main(String[] args) {
    auth();
  }
}